'use strict';

/* Filters */
// need load the moment.js to use this filter. 
angular.module('app')
    .filter('OtherUserFriendShipStatusFilter',
        ['FRIENDSHIP', function (FRIENDSHIP) {

            return function (friend_ship_status) {

                console.log(friend_ship_status);

                switch (friend_ship_status) {
                    case FRIENDSHIP.STATUS.REQUEST_NOT_SEND:
                        return FRIENDSHIP.STATUS_STRING.REQUEST_NOT_SEND;
                    case FRIENDSHIP.STATUS.REQUEST_ACCEPTED:
                        return FRIENDSHIP.STATUS_STRING.REQUEST_ACCEPTED;
                    case FRIENDSHIP.STATUS.REQUEST_PENDING:
                        return FRIENDSHIP.STATUS_STRING.REQUEST_PENDING;
                    case FRIENDSHIP.STATUS.REQUEST_SEND:
                        return FRIENDSHIP.STATUS_STRING.REQUEST_SEND;
                }
            }
        }]
    )