// config

var app =
    angular.module('app')
        .run(
            ['$rootScope', '$location', 'environmentService', 'environmentService', 'APP_INFO_',
                function ($rootScope, $location, environmentService, APP_INFO_) {

                    // check if the user is logged in and page is restricted without login
                    $rootScope.$on('$stateChangeSuccess', function (e, toState, toParams, fromState, fromParams) {
                        e.preventDefault();

                        environmentService.setEnvironment($location.host());
                    });
                }
            ]
        )
        .config(
            function (envServiceProvider, APP_INFO_) {
                // set the domains and variables for each environment
                envServiceProvider.config({
                    domains: {
                        development: ['localhost', 'administration-summmmit.c9users.io'],
                        staging: ['162.243.57.151'],
                        production: ['acme.com']
                        // anotherStage: ['domain1', 'domain2']
                    },
                    vars: {
                        development: {
                            HOST: APP_INFO_.HOST_INFO.HOST,
                            PORT: APP_INFO_.HOST_INFO.PORT,
                            API_URL: APP_INFO_.HOST_INFO.API_URL,
                            STATIC_URL: APP_INFO_.HOST_INFO.STATIC_URL
                        },
                        staging: {
                            HOST: '162.243.57.151',
                            PORT: '8080',
                            API_URL: 'http://162.243.57.151:8080',
                            STATIC_URL: '//static.acme.com'
                        },
                        production: {
                            HOST: '162.243.57.151',
                            PORT: '8080',
                            API_URL: 'http://162.243.57.151:8080',
                            STATIC_URL: '//static.acme.com'
                        }
                    }
                });
                envServiceProvider.check();
            })
        .service('environmentService', ['envService',
            function (envService) {

                return {
                    setEnvironment: function (host) {

                        if (host == "162.243.57.151") {
                            envService.set('staging');
                        }
                        else if (host == "administration-summmmit.c9users.io") {
                            envService.set('development');
                        }
                    }
                }
            }
        ]);
