// persisting email as _user in the cookies for the user to identify in the sessions.
app.service('AdminRolesService', ['$http', '$q', 'API_TYPE', 'GetURLFactory',
    function ($http, $q, API_TYPE, GetURLFactory) {

        return {
            getAllRoles: function (limit, offset) {
                var defer = $q.defer();

                $http.get(GetURLFactory.getURL() + API_TYPE._ADMIN_.ROLES, {
                    params: {
                        limit: limit,
                        offset: offset
                    }
                })
                    .then(
                        // success
                        function (response) {
                            if (response.data.result.statusCode == 200) {
                                defer.resolve(response.data.result.data);
                            }
                        },
                        // failed
                        function (response) {
                            //user = false;
                            defer.reject(response.data.result);
                        }
                    );
                return defer.promise;
            },
            putRoles: function (role) {

                var defer = $q.defer();
                $http.put(GetURLFactory.getURL() + API_TYPE._ADMIN_.ROLES + '/' + role._id, role)
                    .then(
                        // success
                        function (response) {
                            console.log(response);
                            if (response.data.result.statusCode == 200) {
                                defer.resolve(response.data.result);
                            }
                        },
                        // failed
                        function (response) {
                            //user = false;
                            defer.reject(response.data.result);
                        }
                    );
                return defer.promise;
            },
            postRoles: function (role) {
                console.log(role);
                var defer = $q.defer();
                $http.post(GetURLFactory.getURL() + API_TYPE._ADMIN_.ROLES, role)
                    .then(
                        // success
                        function (response) {
                            if (response.data.result.statusCode == 200) {
                                defer.resolve(response.data.result);
                            }
                        },
                        // failed
                        function (response) {
                            //user = false;
                            defer.reject(response.data.result);
                        }
                    );
                return defer.promise;
            },
            deleteRoles: function (role) {

                var defer = $q.defer();
                $http.delete(GetURLFactory.getURL() + API_TYPE._ADMIN_.ROLES + '/' + role._id)
                    .then(
                        // success
                        function (response) {
                            if (response.data.result.statusCode == 200) {
                                defer.resolve(response.data.result);
                            }
                        },
                        // failed
                        function (response) {
                            //user = false;
                            defer.reject(response.data.result);
                        }
                    );
                return defer.promise;
            },
        }
    }
])
