'use strict';

app.controller('AuthenticationReLoginController', ['$rootScope', '$scope', 'AuthService', 'InstallationService', '$state',
'$stateParams', 'HTTP_CODES', '$timeout', '$http', '$window', 'UserPersistenceService', 'HomeService',
    function ($rootScope, $scope, AuthService, InstallationService, $state, $stateParams,
        HTTP_CODES, $timeout, $http, $window, UserPersistenceService, HomeService) {

        $scope.alerts = [];

        console.log($rootScope.user);

        HomeService.getUserInfo()
            .then(function (response) {
                $scope.user_info = HomeService.userUtility(response);
            });

        if ($stateParams.alertParam) {
            $scope.alerts.push({
                type: 'danger',
                msg: $stateParams.alertParam.statusText
            });
        }

        $scope.closeAlert = function (index) {
            $scope.alerts.splice(index, 1);
        };

        function closeEarlierAlert() {
            $scope.alerts.splice(0, 1);
        }

        // unlock the sleeping user
        $scope.submitReSignIn = function (relogin) {

            relogin.refresh_token = UserPersistenceService.getRefreshToken();
            relogin.email = $scope.user_info.email;

            AuthService.reAuthenticateUser(relogin)
                .then(function (response) {
                    $state.transitionTo('page.home', {
                        userParam: response
                    });
                }, function (response) {
                    console.info(response)
                })
        }
        $scope.submitLogOut = function () {
            AuthService.logout()
            closeEarlierAlert();
            $rootScope.user = undefined;
            $state.transitionTo('authentication.signin', {
                alertParam: {
                    'statusText': 'Logged Out!!'
                }
            });
            sessionStorage.clear();
        }
    }
]);
