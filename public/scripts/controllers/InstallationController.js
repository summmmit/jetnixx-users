'use strict';

app.controller('InstallationController', ['$rootScope', '$scope', 'AuthService', 'InstallationService', '$state', '$stateParams', 'HTTP_CODES', '$timeout',
    function ($rootScope, $scope, AuthService, InstallationService, $state, $stateParams, HTTP_CODES, $timeout) {

        $scope.alerts = [];
        $scope.admin_created = false;
        $scope.client_created = false;
        $scope.database_updated = false;
        $scope.database_connection = false;
        $scope.server_connection = false;

        var admin = {
            first_name: "Admin",
            last_name: "test",
            email: "admin@admin.xx",
            password: "00000000",
            country_code: "91",
            contact_number: "99999999",
        }

        console.info($stateParams.installation_step)

        if ($stateParams.installation_step == 1) {

            // function for checking database connection
            InstallationService.getDatabaseConnection()
                .then(function (response) {
                    console.log(response)
                    if (response.statusCode == HTTP_CODES.SUCCESS.OK) {
                        $timeout(function () {
                            $scope.setDatabaseConnection(response.data.database);
                        }, 1000)
                    }
                    else {
                        $scope.alerts.push({
                            type: 'danger',
                            msg: 'Something Went wrong. Please Check Database Settings again!!'
                        });
                    }
                }, function (response) {
                    console.log(response)
                })
        }
        else if ($stateParams.installation_step == 2) {

            InstallationService.getHostConnection()
                .then(function (response) {
                    if (response.statusCode == HTTP_CODES.SUCCESS.OK) {
                        $timeout(function () {
                            $scope.setServerConnection(response.data.server);
                        }, 1000)
                    }
                    else {
                        $scope.alerts.push({
                            type: 'danger',
                            msg: 'Something Went wrong. Please Check Settings Settings again!!'
                        });
                    }
                })

        }
        else if ($stateParams.installation_step == 3) {

            AuthService.register(admin)
                .then(function (response) {
                    if (response.statusCode == HTTP_CODES.SUCCESS.OK) {
                        $timeout(function () {
                            $scope.setAdminCreated(admin);

                            var user_id = response.data.user._id;
                            var client = {
                                user_id: user_id,
                                redirect_url: 'http://localhost_41:8080',
                                client_name: 'Administration App_41',
                                client_type: 2,
                                is_revoked: 0,
                            }

                            InstallationService.createOAuthClient(client)
                                .then(function (response) {
                                    console.log(response)
                                    if (response.statusCode == HTTP_CODES.SUCCESS.OK) {
                                        $timeout(function () {

                                            $scope.setClientCreated(response.data);
                                            InstallationService.postInstallApp()
                                                .then(function (response) {
                                                    console.log(response)
                                                    if (response.statusCode == HTTP_CODES.SUCCESS.OK) {
                                                        $timeout(function () {
                                                            $scope.setDatabaseUpdated();
                                                        }, 1000);
                                                    }
                                                    else {
                                                        $scope.alerts.push({
                                                            type: 'danger',
                                                            msg: response.statusText
                                                        });
                                                    }
                                                })
                                        }, 2000)
                                    }
                                    else {
                                        $scope.alerts.push({
                                            type: 'danger',
                                            msg: response.statusText
                                        });
                                    }
                                })

                        }, 2000)
                    }
                    else {
                        $scope.alerts.push({
                            type: 'danger',
                            msg: response.statusText
                        });
                    }
                })
        }

        $scope.setDatabaseConnection = function (database) {
            $scope.database_connection = true;
            $scope.database = database;
        }
        $scope.setServerConnection = function (server) {
            $scope.server_connection = true;
            $scope.server = server;
        }

        $scope.setAdminCreated = function (admin) {
            $scope.admin_created = true;
            $scope.admin = admin;
        }

        $scope.setClientCreated = function (client) {
            $scope.client_created = true;
            $scope.client = client;
        }

        $scope.setDatabaseUpdated = function () {
            $scope.database_updated = true;
        }

        $scope.createAdminUser = function () {}
    }
]);
