app.controller('AdminPermissionsController', ['$scope', '$timeout', 'AdminPermissionsService', '$mdDialog', 'showToastService', 'HTTP_CODES', '$filter', 'AdminRolesService',
    function ($scope, $timeout, AdminPermissionsService, $mdDialog, showToastService, HTTP_CODES, $filter, AdminRolesService) {

        AdminPermissionsService.getAllPermissions()
            .then(function (response) {
                $scope.Permissions = response.permissions;
            })

        $scope.viewPermissionDialog = function (ev) {
            $mdDialog.show({
                templateUrl: 'views/application/admin/partials/permission_view.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen, // Only for -xs, -sm breakpoints.
                locals: {
                    permission: ev
                },
                controller: viewPermissionController
            })
                .then(function (permission) {
                    AdminPermissionsService.putPermissions(permission)
                        .then(function (response) {
                            if (response.statusCode == HTTP_CODES.SUCCESS.OK) {
                                //$scope.clients.push(response.data);
                                showToastService.showSimpleToast('SuccessFully edited!!');
                            } else {
                                showToastService.showSimpleToast('Problem in edited Permission right now!!');
                            }
                        });
                }, function (response) {
                    // cancelled dialog
                });
        }
        function viewPermissionController($scope, $mdDialog, permission) {
            $scope.Permission = permission;
            AdminRolesService.getAllRoles()
                .then(function (response) {
                    $scope.Roles = response.Roles;
                })
            $scope.edit = true;
            $scope.hide = function () {
                $mdDialog.hide();
            };
            $scope.cancel = function () {
                $mdDialog.cancel();
            };
            $scope.toogleRoleToPermission = function(role, permission){
                console.log(role)
                console.log(permission)
                AdminPermissionsService.postAssignPermissionToRole(role, permission)
                    .then(function (response) {
                        console.log(response)
                    })
            }
        }
        $scope.editPermissionsDialog = function (ev) {
            $mdDialog.show({
                templateUrl: 'views/application/admin/partials/new_permission.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen, // Only for -xs, -sm breakpoints.
                locals: {
                    permission: ev
                },
                controller: function ($scope, $mdDialog, permission) {
                    $scope.permission = permission;
                    $scope.edit = true;
                    $scope.hide = function () {
                        $mdDialog.hide();
                    };

                    $scope.cancel = function () {
                        $mdDialog.cancel();
                    };

                    $scope.createPermission = function (permission) {
                        $mdDialog.hide(permission);
                    };
                }
            })
                .then(function (permission) {
                    AdminPermissionsService.putPermissions(permission)
                        .then(function (response) {
                            if (response.statusCode == HTTP_CODES.SUCCESS.OK) {
                                //$scope.clients.push(response.data);
                                showToastService.showSimpleToast('SuccessFully edited!!');
                            } else {
                                showToastService.showSimpleToast('Problem in edited Permission right now!!');
                            }
                        });
                }, function (response) {
                    // cancelled dialog
                });
        }
        $scope.deletePermission = function (permission) {
            AdminPermissionsService.deletePermissions(permission)
                .then(function (response) {
                    if (response.statusCode == HTTP_CODES.SUCCESS.OK) {
                        var foundItem = $filter('filter')($scope.Permissions, {_id: response.data._id}, true)[0];
                        var index = $scope.Permissions.indexOf(foundItem);
                        $scope.Permissions.splice(index, 1);
                        showToastService.showSimpleToast('SuccessFully Deleted!!');
                    } else {
                        showToastService.showSimpleToast('Problem in Creating right now!!');
                    }
                });
        }
        $scope.newRoleDialog = function (ev) {
            $mdDialog.show({
                controller: createNewRoleController,
                templateUrl: 'views/application/admin/partials/new_permission.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            })
                .then(function (permission) {
                    AdminPermissionsService.postPermissions(permission)
                        .then(function (response) {
                            if (response.statusCode == HTTP_CODES.SUCCESS.OK) {
                                var new_permission = response.data;
                                new_permission.user_permission = [];
                                $scope.Permissions.push(new_permission);
                                showToastService.showSimpleToast('SuccessFully Created!!');
                            } else {
                                showToastService.showSimpleToast('Problem in Creating Permission right now!!');
                            }
                        });
                }, function (response) {
                    // cancelled dialog
                });
        };
        function createNewRoleController($scope, $mdDialog) {
            $scope.hide = function () {
                $mdDialog.hide();
            };

            $scope.cancel = function () {
                $mdDialog.cancel();
            };

            $scope.createPermission = function (permission) {
                $mdDialog.hide(permission);
            };
        }
    }
]);