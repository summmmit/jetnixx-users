app.controller('ClientsController', ['ClientsService', '$scope', '$filter', '$http', 'editableOptions', 'editableThemes', '$mdDialog', 'showToastService', 'HTTP_CODES',
    function (ClientsService, $scope, $filter, $http, editableOptions, editableThemes, $mdDialog, showToastService, HTTP_CODES) {
        editableThemes.bs3.inputClass = 'input-sm';
        editableThemes.bs3.buttonsClass = 'btn-sm';
        editableOptions.theme = 'bs3';

        ClientsService.getAllClients()
            .then(function (response) {
                $scope.clients = response.oauthClients;
            });

        $scope.editClientDialog = function (ev) {
            $mdDialog.show({
                templateUrl: 'views/application/admin/partials/new_client.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen, // Only for -xs, -sm breakpoints.
                locals: {
                    client: ev
                },
                controller: function ($scope, $mdDialog, client) {

                    $scope.client = client;
                    $scope.edit = true;

                    $scope.hide = function () {
                        $mdDialog.hide();
                    };

                    $scope.cancel = function () {
                        $mdDialog.cancel();
                    };

                    $scope.createClient = function (client) {
                        console.log(client)
                        $mdDialog.hide(client);
                    };
                }
            })
                .then(function (client) {

                    var data_to_change = {
                        redirect_url: client.redirect_url,
                        is_revoked: client.is_revoked
                    }

                    ClientsService.patchClients(data_to_change, client._id)
                        .then(function (response) {
                            if (response.statusCode == HTTP_CODES.SUCCESS.OK) {
                                //$scope.clients.push(response.data);
                                showToastService.showSimpleToast('SuccessFully Created Application!!');
                            } else {
                                showToastService.showSimpleToast('Problem in Creating Application right now!!');
                            }
                        });
                }, function (response) {
                    // cancelled dialog
                });
        };

        function editClientController($scope, $mdDialog) {
            $scope.hide = function () {
                $mdDialog.hide();
            };

            $scope.cancel = function () {
                $mdDialog.cancel();
            };

            $scope.createClient = function (client) {
                $mdDialog.hide(client);
            };
        }

        $scope.newClientDialog = function (ev) {
            $mdDialog.show({
                controller: createNewClientController,
                templateUrl: 'views/application/admin/partials/new_client.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            })
                .then(function (client) {
                    ClientsService.postClients(client)
                        .then(function (response) {
                            if (response.statusCode == HTTP_CODES.SUCCESS.OK) {
                                $scope.clients.push(response.data);
                                showToastService.showSimpleToast('SuccessFully Created Application!!');
                            } else {
                                showToastService.showSimpleToast('Problem in Creating Application right now!!');
                            }
                        });
                }, function (response) {
                    // cancelled dialog
                });
        };

        function createNewClientController($scope, $mdDialog) {
            $scope.hide = function () {
                $mdDialog.hide();
            };

            $scope.cancel = function () {
                $mdDialog.cancel();
            };

            $scope.createClient = function (client) {
                $mdDialog.hide(client);
            };
        }

        $scope.deleteClient = function (client) {
            ClientsService.deleteClients(client)
                .then(function (response) {
                    if (response.statusCode == HTTP_CODES.SUCCESS.OK) {
                        var foundItem = $filter('filter')($scope.clients, {_id: response.data._id}, true)[0];
                        var index = $scope.clients.indexOf(foundItem);
                        $scope.clients.splice(index, 1);
                        showToastService.showSimpleToast('SuccessFully Deleted Application!!');
                    } else {
                        showToastService.showSimpleToast('Problem in Creating Application right now!!');
                    }
                });
        }

        $scope.items = ['Item 1', 'Item 2', 'Item 3'];

        $scope.revoked = [
            {
                value: 1,
                text: 'Not Revoked'
            },
            {
                value: 2,
                text: 'Revoked'
            }
        ];

        $scope.showRevoked = function () {
            var selected = $filter('filter')($scope.revoked, {
                value: $scope.client.revoked
            });
            console.log(client);
            console.log(selected);
            return ($scope.client.revoked && selected.length) ? selected[0].text : 'Not set';
        };

        $scope.user = {
            name: 'awesome',
            desc: 'Awesome user \ndescription!',
            status: 2,
            agenda: 1,
            remember: false
        };

        $scope.statuses = [
            {
                value: 1,
                text: 'status1'
            },
            {
                value: 2,
                text: 'status2'
            },
            {
                value: 3,
                text: 'status3'
            }
        ];

        $scope.agenda = [
            {
                value: 1,
                text: 'male'
            },
            {
                value: 2,
                text: 'female'
            }
        ];

        $scope.showStatus = function () {
            var selected = $filter('filter')($scope.statuses, {
                value: $scope.user.status
            });
            return ($scope.user.status && selected.length) ? selected[0].text : 'Not set';
        };

        $scope.showAgenda = function () {
            var selected = $filter('filter')($scope.agenda, {
                value: $scope.user.agenda
            });
            return ($scope.user.agenda && selected.length) ? selected[0].text : 'Not set';
        };

        // editable table
        $scope.users = [
            {
                id: 1,
                name: 'awesome user1',
                status: 2,
                group: 4,
                groupName: 'admin'
            },
            {
                id: 2,
                name: 'awesome user2',
                status: undefined,
                group: 3,
                groupName: 'vip'
            },
            {
                id: 3,
                name: 'awesome user3',
                status: 2,
                group: null
            }
        ];

        $scope.groups = [];
        $scope.loadGroups = function () {
            return $scope.groups.length ? null : $http.get('api/groups').success(function (data) {
                    $scope.groups = data;
                });
        };

        $scope.showGroup = function (user) {
            if (user.group && $scope.groups.length) {
                var selected = $filter('filter')($scope.groups, {
                    id: user.group
                });
                return selected.length ? selected[0].text : 'Not set';
            }
            else {
                return user.groupName || 'Not set';
            }
        };

        $scope.showStatus = function (user) {
            var selected = [];
            if (user && user.status) {
                selected = $filter('filter')($scope.statuses, {
                    value: user.status
                });
            }
            return selected.length ? selected[0].text : 'Not set';
        };

        $scope.checkName = function (data, id) {
            if (id === 2 && data !== 'awesome') {
                return "Username 2 should be `awesome`";
            }
        };

        $scope.saveUser = function (data, id) {
            //$scope.user not updated yet
            angular.extend(data, {
                id: id
            });
            // return $http.post('api/saveUser', data);
        };

        // remove user
        $scope.removeUser = function (index) {
            $scope.users.splice(index, 1);
        };

        // add user
        $scope.addUser = function () {
            $scope.inserted = {
                id: $scope.users.length + 1,
                name: '',
                status: null,
                group: null
            };
            $scope.users.push($scope.inserted);
        };

    }]);
