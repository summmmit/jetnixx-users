'use strict';

app.controller('AuthenticationController', ['$rootScope', '$scope', 'AuthService', 'InstallationService', '$state',
'$stateParams', 'HTTP_CODES', '$timeout', '$http', '$window', 'UserPersistenceService', 'HomeService',
    function ($rootScope, $scope, AuthService, InstallationService, $state, $stateParams,
        HTTP_CODES, $timeout, $http, $window, UserPersistenceService, HomeService) {

        $scope.alerts = [];

        if ($stateParams.alertParam) {
            $scope.alerts.push({
                type: 'danger',
                msg: $stateParams.alertParam.statusText
            });
        }

        $scope.closeAlert = function (index) {
            $scope.alerts.splice(index, 1);
        };

        function closeEarlierAlert() {
            $scope.alerts.splice(0, 1);
        }

        // function to run when signup button is clicked
        $scope.submitSignUp = function ($user) {
                AuthService.register($user)
                    .then(function (response) {
                        closeEarlierAlert();
                        if (response.statusCode == HTTP_CODES.CLIENT_ERROR.CONFLICT ||
                            response.statusCode == HTTP_CODES.SERVER_ERROR.INTERNAL_SERVER_ERROR) {

                            $scope.alerts.push({
                                type: 'danger',
                                msg: response.statusText
                            });
                        }
                        else if (response.statusCode == HTTP_CODES.SUCCESS.OK) {
                            $state.transitionTo('authentication.signin', {
                                alertParam: response
                            });
                        }
                        else {
                            $scope.alerts.push({
                                type: 'danger',
                                msg: 'Something Went wrong!!'
                            });
                        }
                    })
            }
            // function to run when signup button is clicked
        $scope.submitSignIn = function (member) {

                AuthService.login(member)
                    .then(function (response) {
                        closeEarlierAlert();
                        if (response.status == HTTP_CODES.SUCCESS.OK) {
                            $scope.sendLoginDetailsToServer();
                            $state.transitionTo('page.home', {
                                userParam: response
                            });
                        }
                    }, function (response) {
                        closeEarlierAlert();
                        $scope.alerts.push({
                            type: 'danger',
                            msg: response.data.message
                        });
                        $scope.member.password = '';
                    })
            }
            // send the login details to server
        $scope.sendLoginDetailsToServer = function () {
                $http.jsonp("https://ipinfo.io/json?format=jsonp&callback=JSON_CALLBACK")
                    .then(function (response) {
                        var user_access_details = {
                            ip_address: response.data.ip,
                            user_agent: $window.navigator.userAgent,
                            login_at: new Date(),
                            _access_token: UserPersistenceService.getAccessToken()
                        }
                        AuthService.postUserAccessDetails(user_access_details);
                    })
            }
            // function to run when logout is clicked
        $scope.logOut = function () {
                AuthService.logout()
                closeEarlierAlert();
                $rootScope.user = undefined;
                $state.transitionTo('authentication.signin', {
                    alertParam: {
                        'statusText': 'Logged Out!!'
                    }
                });
                sessionStorage.clear();
            }
            // check email is valid or not
        $scope.forgotPassword = function () {
                authenticationService.forgotPassword($scope.email)
                    .then(function (response) {
                        closeEarlierAlert();
                        if (response.statusCode == HTTP_CODES.CLIENT_ERROR.BAD_REQUEST || response.statusCode == HTTP_CODES.CLIENT_ERROR.NOT_ACCEPTABLE ||
                            response.statusCode == HTTP_CODES.SUCCESS.NON_AUTHORITATIVE_INFORMATION || response.statusCode == HTTP_CODES.SERVER_ERROR.INTERNAL_SERVER_ERROR) {

                            $scope.alerts.push({
                                type: 'danger',
                                msg: response.statusText
                            });
                        }
                        else if (response.statusCode == HTTP_CODES.SUCCESS.OK) {
                            $state.transitionTo('authentication.signin', {
                                alertParam: response
                            });
                        }
                        else {
                            $scope.alerts.push({
                                type: 'danger',
                                msg: 'something Went wrong!!'
                            });
                        }
                    })
            }
            // change password
        $scope.changePassword = function () {
                var new_password = $scope.user.new_password;
                var confirm_password = $scope.user.confirm_password;

                if (new_password == confirm_password) {

                    authenticationService.forgotPasswords($scope.email)
                        .then(function (response) {
                            closeEarlierAlert();
                            if (response.statusCode == HTTP_CODES.CLIENT_ERROR.NOT_ACCEPTABLE) {
                                console.log(response);
                            }
                            else if (response.statusCode == HTTP_CODES.CLIENT_ERROR.CONFLICT || response.statusCode == HTTP_CODES.CLIENT_ERROR.BAD_REQUEST ||
                                response.statusCode == HTTP_CODES.SUCCESS.NON_AUTHORITATIVE_INFORMATION) {

                                $scope.alerts.push({
                                    type: 'danger',
                                    msg: response.statusText
                                });
                            }
                            else if (response.statusCode == HTTP_CODES.SUCCESS.OK) {
                                $state.transitionTo('page.home', {
                                    userParam: response
                                });
                            }
                            else {
                                $scope.alerts.push({
                                    type: 'danger',
                                    msg: 'something Went wrong!!'
                                });
                            }
                        })
                }
                $scope.alerts.push({
                    type: 'danger',
                    msg: 'Passwords are not same.!!'
                });
            }
    }
]);
