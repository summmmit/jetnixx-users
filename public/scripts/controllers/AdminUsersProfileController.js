app.controller('AdminUsersProfileController', ['$scope', '$stateParams', '$timeout', 'AdminUsersService', '$mdDialog', 'showToastService', 'HTTP_CODES', '$filter', 'AuthService',
    function ($scope, $stateParams, $timeout, AdminUsersService, $mdDialog, showToastService, HTTP_CODES, $filter, AuthService) {

        AdminUsersService.getUserByUserId($stateParams.user_id)
            .then(function (response) {
                $scope.user_profile = response;
                $scope.user_profile.user_details.dob = $scope.changeToDate(response.user_details.dob);
            })
        $scope.getUserSessions = function () {
            AdminUsersService.getActiveSessionUsers($stateParams.user_id)
                .then(function (response) {
                    $scope.UserSessions = response;
                    $timeout(function () {
                        $('.table').trigger('footable_redraw');
                    }, 100);
                })
        }
        $scope.changeUserStatusDialog = function (ev) {
            $mdDialog.show({
                    controller: changeUserStatusController,
                    templateUrl: 'views/application/admin/partials/change_user_status.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: $scope.customFullscreen, // Only for -xs, -sm breakpoints.
                    locals: {
                        user_status: ev
                    },
                })
                .then(function (status) {
                    AdminUsersService.putChangeUserStatus($stateParams.user_id, status)
                        .then(function (response) {
                            if (response.statusCode == HTTP_CODES.SUCCESS.OK) {
                                $scope.user_profile.activated = response.data.activated;
                                showToastService.showSimpleToast(response.statusText);
                            }
                            else {
                                showToastService.showSimpleToast('Problem in Changing Status right now!!');
                            }
                        });
                }, function (response) {
                    // cancelled dialog
                });
        }

        function changeUserStatusController($scope, $mdDialog, user_status) {
            $scope.user_status = user_status;
            $scope.hide = function () {
                $mdDialog.hide();
            };
            $scope.cancel = function () {
                $mdDialog.cancel();
            };
            $scope.userChangeStatus = function (status) {
                $mdDialog.hide(status);
            };
        }
        $scope.UserSessionInvalidate = function (access_token) {
            var foundItem = $filter('filter')($scope.UserSessions, {
                access_token: access_token
            }, true)[0];
            var index = $scope.UserSessions.indexOf(foundItem);
            $scope.UserSessions.splice(index, 1);
            $timeout(function () {
                $('.table').trigger('footable_redraw');
            }, 100);
        }
        $scope.changeMemberInfo = function (user_profile) {
            var user_info = {
                first_name: user_profile.user_details.first_name,
                last_name: user_profile.user_details.last_name,
                sex: user_profile.user_details.sex,
                dob: user_profile.user_details.dob,
                nick_name: user_profile.user_details.nick_name
            }
            AdminUsersService.putAdminUserInfo($stateParams.user_id, user_info)
                .then(function (response) {
                    if (response.statusCode == HTTP_CODES.SUCCESS.OK) {
                        showToastService.showSimpleToast(response.statusText);
                    }
                    else {
                        showToastService.showSimpleToast('Problem in Updating right now!!');
                    }
                })
        }
        $scope.changeToDate = function (input) {
            return new Date(input);
        }
        $scope.setNewContactNumber = function (ev) {
            $mdDialog.show({
                    controller: changeContactNumber,
                    templateUrl: 'views/application/admin/partials/change_contact_number.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                })
                .then(function (user_profile) {
                    AdminUsersService.putContactNumber($stateParams.user_id, user_profile)
                        .then(function (response) {
                            if (response.statusCode == HTTP_CODES.SUCCESS.OK) {
                                $scope.user_profile.contact_number = response.data.contact_number;
                                $scope.user_profile.country_code = response.data.country_code;
                                showToastService.showSimpleToast(response.statusText);
                            }
                            else {
                                showToastService.showSimpleToast(response.statusText);
                            }
                        }, function (response) {
                            showToastService.showSimpleToast(response.statusText);
                        })
                });
        };
        $scope.setNewPrimaryEmail = function (ev) {

            $mdDialog.show({
                    controller: changePrimaryEmail,
                    templateUrl: 'views/application/admin/partials/change_primary_email.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                })
                .then(function (user_profile) {
                    AdminUsersService.putEmailAddress($stateParams.user_id, user_profile)
                        .then(function (response) {
                            if (response.statusCode == HTTP_CODES.SUCCESS.OK) {
                                $scope.user_profile.email = response.data.email;
                                showToastService.showSimpleToast(response.statusText);
                            }
                            else {
                                showToastService.showSimpleToast(response.statusText);
                            }
                        }, function (response) {
                            showToastService.showSimpleToast(response.statusText);
                        })
                });
        };
        $scope.setNewPassword = function (ev) {

            $mdDialog.show({
                    controller: changePassword,
                    templateUrl: 'views/application/admin/partials/change_password.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                })
                .then(function (user_profile) {
                    AdminUsersService.postChangePassword($stateParams.user_id, user_profile)
                        .then(function (response) {
                            if (response.statusCode == HTTP_CODES.SUCCESS.OK) {
                                showToastService.showSimpleToast(response.statusText);
                            }
                            else {
                                showToastService.showSimpleToast(response.statusText);
                            }
                        }, function (response) {
                            showToastService.showSimpleToast(response.statusText);
                        })
                });
        };
        $scope.showConfirmDelete = function (ev) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title('Confirm Account Deletion')
                .textContent('Would you like to delete your Account??')
                .ariaLabel('Bad Day')
                .targetEvent(ev)
                .ok('Yes!!')
                .cancel('Cancel!!');
            $mdDialog.show(confirm)
                .then(function (user) {

                    AdminUsersService.deleteUser()
                        .then(function (response) {

                            if (response.statusCode == HTTP_CODES.SUCCESS.OK) {
                                AuthService.logout();
                                $state.go('authentication.signin', {
                                    alertParam: {
                                        'statusText': response.statusText
                                    }
                                });
                            }
                            else {
                                showToastService.showSimpleToast(response.statusText);
                            }

                        }, function (response) {
                            showToastService.showSimpleToast(response.statusText);
                        })
                });
        };
                }])

function changeContactNumber($scope, $mdDialog) {
    $scope.userState = '';
    $scope.states = ('IND ( + 91), JP ( + 81) ').split(',').map(function (state) {
        return {
            abbrev: state
        };
    });
    $scope.hide = function () {
        $mdDialog.hide();
    };
    $scope.cancel = function () {
        $mdDialog.cancel();
    };
    $scope.changeNumber = function (user_profile) {
        $mdDialog.hide(user_profile);
    };
}

function changePrimaryEmail($scope, $mdDialog) {
    $scope.hide = function () {
        $mdDialog.hide();
    };
    $scope.cancel = function () {
        $mdDialog.cancel();
    };
    $scope.changeEmail = function (user_profile) {
        $mdDialog.hide(user_profile);
    };

}

function changePassword($scope, $mdDialog) {
    $scope.hide = function () {
        $mdDialog.hide();
    };
    $scope.cancel = function () {
        $mdDialog.cancel();
    };
    $scope.changeUserPassword = function (user_profile) {
        $mdDialog.hide(user_profile);
    };
}

app.filter('UserStatus', function () {
    return function (status) {

        if (status == 0) {
            return 'Not Activated';
        }
        else if (status == 1) {
            return 'Active';
        }
        else if (status == 2) {
            return 'Disabled';
        }
        else if (status == 3) {
            return 'Suspended';
        }
    }
});
