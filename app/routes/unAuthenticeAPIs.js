var express = require('express'),
    router = express.Router(),
    userAccessValidator = require('./../controllers/parameterValidators').userAccessValidator,
    userDetailsController = require('./../controllers/UserDetailsController'),
    UserRolesController = require('./../controllers').UserRolesController,
    InstallationController = require('./../controllers').InstallationController,
    OAuthAccessTokenController = require('./../controllers').OAuthAccessTokenController;

router.post('/login', userDetailsController.postLogin);
router.post('/database_connection', InstallationController.getCheckDatabaseConnection);
router.post('/server_connection', InstallationController.postHostConnection);
router.get('/install_app/:current_version', InstallationController.checkIfInstalledApp);
router.post('/install_app', InstallationController.postInstallApp);

// User Registration
router.post('/user', userAccessValidator.signupValidator, userDetailsController.postMember)
      .post('/user/authenticate_password', userDetailsController.postAuthenticatePassword)

module.exports = router;
