var express = require('express'),
    router = express.Router(),
    AuthenticationTokenAPI = require('./../api/authenticationToken_api'),
    userAccessValidator = require('./../controllers/parameterValidators/userAccessValidator'),
    userDetailsController = require('./../controllers/UserDetailsController');

// Token API
router.post('/x/token', userAccessValidator.loginValidator, AuthenticationTokenAPI.getToken);

module.exports = router;