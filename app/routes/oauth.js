var express = require('express'),
    router = express.Router();
var userAccessController = require('./../controllers/UserAccessController');
var OAuthController = require('./../controllers/OAuthClientsController');
var OAuthCodeController = require('./../controllers/OAuthCodesController');
var OAuthAccessTokenController = require('./../controllers/OAuthAccessTokenController');
var AuthenticationTokenAPI = require('./../api/authenticationToken_api');
var OAuthValidator = require('./../controllers/parameterValidators').OAuthValidator;

//--------------------------Client API ------------------------------------------------
router
    .post('/oauth/clients', OAuthValidator.checkPostOAuthClient, OAuthController.postOAuthClients)
    .get('/oauth/clients', OAuthController.getOAuthClients)
    .patch('/oauth/clients', OAuthController.patchOAuthClients)
    .delete('/oauth/clients', OAuthController.deleteOAuthClients)
    .get('/oauth/clients/:client_id', OAuthController.authenticateOAuthClients);

router.all('/oauth/codes')
    .post(OAuthCodeController.postOAuthCodes)
    .get(OAuthCodeController.getOAuthCodes)
    .delete(OAuthCodeController.deleteOAuthCodes);

module.exports = router;
