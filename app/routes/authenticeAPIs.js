var express = require('express'),
    router = express.Router(),
    userAccessValidator = require('./../controllers/parameterValidators').userAccessValidator,
    userDetailsController = require('./../controllers/UserDetailsController'),
    UserAccessDetailsController = require('./../controllers/UserAccessDetailsController'),
    UserRolesController = require('./../controllers').UserRolesController,
    SettingsController = require('./../controllers').SettingsController,
    UserAccessController = require('./../controllers').UserAccessController,
    AdminPermissionsController = require('./../controllers').AdminPermissionsController,
    OAuthUserApplicationsController = require('./../controllers').OAuthUserApplicationsController,
    AdminUsersController = require('./../controllers').AdminUsersController,
    OAuthAccessTokenController = require('./../controllers').OAuthAccessTokenController;

router.use(OAuthAccessTokenController.authenticateAccessTokenClients,
    OAuthAccessTokenController.getUserFromAccessToken);

router
    .post('/user_access_details', UserAccessDetailsController.postUserAccessDetails)

router
    .get('/user', userDetailsController.getMemberInfo)
    .put('/user', userDetailsController.putMemberInfo)
    .patch('/user/contact_number', UserAccessController.patchChangeContactNumber)
    .patch('/user/email', UserAccessController.patchEmailAddress)
    .get('/user/notifications', SettingsController.getNotifications)
    .put('/user/notifications', SettingsController.putNotifications)
    .post('/user/sessions', UserAccessController.postUserSession)
    .get('/user/sessions', UserAccessController.postUserSession)

// Admin User Routes
router
    .get('/admin/user', AdminUsersController.getAdminUsers)
    .get('/admin/user/:user_id', AdminUsersController.getAdminUserByUserId)
    .put('/admin/user/:user_id', AdminUsersController.putAdminUserInfo)
    .delete('/admin/user/:user_id', UserRolesController.deleteRoles)
    .put('/admin/user/status/:user_id', UserAccessController.putChangeUserStatus)
    .put('/admin/user/contact_number/:user_id', AdminUsersController.putContactNumber)
    .put('/admin/user/email/:user_id', AdminUsersController.putEmailAddress)
    .post('/admin/user/password/:user_id', AdminUsersController.postChangePassword)
    .get('/admin/user/sessions/:user_id', UserAccessDetailsController.getUserAccessDetailsByUserId)

// Admin User Roles Routes
router
    .post('/user/roles', UserRolesController.postRoles)
    .get('/user/roles', UserRolesController.getAllRoles)
    .put('/user/roles/:role_id', UserRolesController.putRoles)
    .delete('/user/roles/:role_id', UserRolesController.deleteRoles)
    .post('/assign/user/roles', UserRolesController.postAssignRoleToUser)

// Admin User Permissions Routes
router
    .post('/admin/permission', AdminPermissionsController.postPermissions)
    .get('/admin/permission', AdminPermissionsController.getAllPermissions)
    .get('/admin/permission/:permission_id', AdminPermissionsController.getPermission)
    .put('/admin/permission/:permission_id', AdminPermissionsController.putPermissions)
    .delete('/admin/permission/:permission_id', AdminPermissionsController.deletePermissions)
    .post('/assign/admin/permission', AdminPermissionsController.postAssignPermissionToRole)
    .get('/admin/permission/roles/:permission_id', AdminPermissionsController.getAllRolesToPermission)

// User Applications
router
    .post('/applications', OAuthUserApplicationsController.postApplication)
    .get('/applications', OAuthUserApplicationsController.getAllApplications)
    .patch('/applications/:application_id', OAuthUserApplicationsController.patchApplication)
    .delete('/applications/:application_id', OAuthUserApplicationsController.deleteApplication)
    .get('/applications/:application_id', OAuthUserApplicationsController.authenticateApplication);

module.exports = router;