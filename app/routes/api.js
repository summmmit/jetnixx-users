var express = require('express'),
    router = express.Router(),
    userDetailsController = require('./../controllers/UserDetailsController'),
    UserRolesController = require('./../controllers').UserRolesController,
    OAuthAccessTokenController = require('./../controllers').OAuthAccessTokenController;
module.exports = function () {

    router.use(require('./unAuthenticeAPIs'));
    return router;
};