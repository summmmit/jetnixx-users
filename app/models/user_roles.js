// load the things we need
var mongoose = require('mongoose');

// define the schema for model
var UserRolesSchema = mongoose.Schema({

        _user_access_id: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            index: true
        },
        role_id: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            index: true
        }
    }
);

// create the model for users and expose it to our app
module.exports = mongoose.model('user_roles', UserRolesSchema);
