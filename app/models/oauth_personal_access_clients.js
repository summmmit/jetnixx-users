// load the things we need
var mongoose = require('mongoose');

// define the schema for our user model
var OAuthPersonalAccessClientsSchema = mongoose.Schema({

        userId: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
        },
        label_name: {
            type: String,
            required: true,
            index: true
        },
        label_icon: {
            type: String,
            required: true
        }
    },
    {
        timestamps: {
            createdAt: 'created_at',
            updatedAt: 'updated_at'
        }
    },
    {
        versionKey: false
    }
);

// create the model for users and expose it to our app
module.exports = mongoose.model('oauth_personal_access_clients', OAuthPersonalAccessClientsSchema);
