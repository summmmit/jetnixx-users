var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var OAuthRefreshTokensSchema = new Schema({
    refresh_token: {
        type: String,
        required: true,
        unique: true
    },
    clientId: String,
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user_access'
    },
    expires: {
        type: Date
    }
});

mongoose.model('oauth_refresh_tokens', OAuthRefreshTokensSchema);
var OAuthRefreshTokensModel = mongoose.model('oauth_refresh_tokens');

module.exports.saveRefreshToken = function (token, clientId, expires, userId, callback) {

    var refreshToken = new OAuthRefreshTokensModel({
        refresh_token: token,
        clientId: clientId,
        userId: userId,
        expires: expires
    });

    refreshToken.save(callback);
};

module.exports.getRefreshToken = function (refreshToken, callback) {
    OAuthRefreshTokensModel.findOne({refresh_token: refreshToken}, function (err, token) {

        // node-oauth2-server defaults to .user or { id: userId }, but { id: userId} doesn't work
        // This is in node-oauth2-server/lib/grant.js on line 256
        if (token) {
            token.user = token.userId;
        }
        callback(err, token);
    });
};
