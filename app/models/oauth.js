var AuthCode = require('./oauth_codes');
var AccessToken = require('./oauth_access_tokens');
var RefreshToken = require('./oauth_refresh_tokens');
var User = require('./user_access');
var Client = require('./oauth_clients');
var OAuthClientsController = require('./../controllers/OAuthClientsController');

// ******************* node-oauth2-server API ****************************************
module.exports.getAuthCode = AuthCode.getAuthCode;
module.exports.saveAuthCode = AuthCode.saveAuthCode;
module.exports.getAccessToken = AccessToken.getAccessToken;
module.exports.saveAccessToken = AccessToken.saveAccessToken;
module.exports.saveRefreshToken = RefreshToken.saveRefreshToken;
module.exports.getRefreshToken = RefreshToken.getRefreshToken;
module.exports.getUser = User.getUser;
module.exports.getClient = Client.getClient;
module.exports.grantTypeAllowed = Client.grantTypeAllowed;
