// load the things we need
var mongoose = require('mongoose');

// define the schema for our user model
var PermissionsRolesSchema = mongoose.Schema({

        permission_id: {
            type: mongoose.Schema.Types.ObjectId,
            index: true,
            required: true,
        },
        role_id: {
            type: mongoose.Schema.Types.ObjectId,
            index: true,
            required: true,
        }
    }
);
// create the model for users and expose it to our app
module.exports = mongoose.model('permissions_roles', PermissionsRolesSchema);
