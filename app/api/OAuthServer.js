var BasicStrategy = require('passport-http').Strategy;
var HTTPBearerStrategy = require('passport-http-bearer').Strategy

var oauth2rize = require('oauth2orize');
var OAuthClients = require('./../models/oauth_clients');
var OAuthAccessTokens = require('./../models/oauth_access_tokens');
var OAuthRefreshTokens = require('./../models/oauth_refresh_tokens');
var OAuthCodes = require('./../models/oauth_codes');
var CONSTANTS = require('./../helpers/constants');
var User = require('./../models/user_access');

var server = oauth2rize.createServer();

// Register serialialization function
server.serializeClient(function (client, callback) {
    return callback(null, client._id);
});

// Register deserialization function
server.deserializeClient(function (id, callback) {
    Client.findOne({_id: id}, function (err, client) {
        if (err) {
            return callback(err);
        }
        return callback(null, client);
    });
});

passport.use('access-token', new HTTPBearerStrategy(
    function (access_token, callback) {
        OAuthAccessTokens.findOne({_id: access_token},
            function (err, oauth_access_token) {

                if (err) {
                    console.log('Error in Authenticating Auth Token');
                    throw err;
                }

                // No token found
                if (!oauth_access_token) {
                    return callback(null, false);
                }

                User.findOne({_id: oauth_access_token._user_access_id}, function (err, user) {

                    if (err) {
                        return callback(err);
                    }

                    // No user found
                    if (!user) {
                        return callback(null, false);
                    }

                    // Simple example with no scope
                    callback(null, user, {scope: '*'});
                });
            })
    }
))
exports.isBearerAuthenticated = passport.authenticate('access-token', {session: false});

passport.use('client-basic', new BasicStrategy(
    function (client_id, client_secret, callback) {
        OAuthClients.findOne({_id: client_id},
            function (err, oauth_clients) {

                if (err) {
                    console.log('Error in Authenticating Auth Code');
                    throw err;
                }

                if (!oauth_clients || client_secret != oauth_clients.validateCode(client_secret)) {
                    return callback(null, false);
                } else {
                    return callback(null, oauth_clients);
                }
            })
    }
));

exports.isClientAuthenticated = passport.authenticate('client-basic', {session: false});