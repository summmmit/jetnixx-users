var NotFoundError = require('./NotFoundError');

module.exports = {
    NotFound: NotFoundError,
};