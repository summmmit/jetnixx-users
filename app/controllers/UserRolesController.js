var userDetails = require('./../models/user_details');
var UserAccess = require('./../models/user_access');
var Notifications = require('./../models/notifications');
// loading user constants
var CONSTANTS = require('./../helpers/constants');
var ResultResponses = require('./../helpers/resultResponses');
var Roles = require('./../models').roles;
var User_To_Roles = require('./../models').user_roles;


module.exports = {

    postRoles: function (req, res) {

        var result = {};

        var name = req.body.name;
        var display_name = req.body.display_name;
        var description = req.body.description;
        var is_removable = req.body.is_removable ? req.body.is_removable : CONSTANTS.ROLE_REMOVABLE.NON_REMOVABLE;
        var _user_access_id = req.member._id;

        var user_roles = new Roles();
        user_roles.name = name;
        user_roles.display_name = display_name;
        user_roles.description = description;
        user_roles.is_removable = is_removable;
        user_roles._user_access_id = _user_access_id;

        user_roles.save(function (err) {
            if (err) {
                console.log('Roles Save Error: RolesController->postRoles');
                throw err;
            }

            result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
                'Successfully Created!!', user_roles);

            res.json({'result': result})
        });
    },
    putRoles: function (req, res) {

        var result = {};

        var name = req.body.name;
        var display_name = req.body.display_name;
        var description = req.body.description;
        var is_removable = req.body.is_removable ? req.body.is_removable : CONSTANTS.ROLE_REMOVABLE.NON_REMOVABLE;

        Roles.findOne({_id: req.params.role_id}, function (err, role) {

            if (err) {
                console.log('Error: RolesController->PutRoles');
                throw err;
            }

            if (!role) {

                result = ResultResponses.success(CONSTANTS.HTTP_CODES.CLIENT_ERROR.BAD_REQUEST,
                    'role does not exists!!', null);
            } else {

                role.name = name;
                role.display_name = display_name;
                role.description = description;
                role.is_removable = is_removable;

                role.save(function (err) {
                    if (err) {
                        console.log('OAuthClients Modification Error: RolesController->PutRoles');
                        throw err;
                    }
                });

                result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
                    'Successfully Modified!!', role);
            }
            res.json({'result': result})
        })
    },
    getAllRoles: function (req, res) {

        var limit = req.query.limit ? req.query.limit : CONSTANTS.API.DEFAULT_LIMIT;
        var offset = req.query.offset ? req.query.offset : CONSTANTS.API.DEFAULT_OFFSET;
        var data, result = {};

        result = ResultResponses.failed(CONSTANTS.HTTP_CODES.SERVER_ERROR.INTERNAL_SERVER_ERROR,
            'Some Error Occurred.');

        var $lookup_array = [
            {
                $lookup: {
                    from: 'user_roles',
                    localField: '_id',
                    foreignField: 'role_id',
                    as: 'user_role'
                }
            },
            {
                $limit: parseInt(limit)
            },
            {
                $skip: parseInt(offset)
            }
        ];

        Roles.aggregate($lookup_array, function (err, roles) {

            if (err) {
                console.log('Error While fetching Roles: RolesController->getAllRoles');
                throw err;
            }

            data = {
                limit: parseInt(limit),
                offset: parseInt(offset)
            }

            Roles.count({}, function (err, TotalCount) {

                if (err) {
                    console.log('Error While fetching Roles count: RolesController->getAllRoles');
                    throw err;
                }

                data.count = TotalCount;
                data.Roles = roles;

                result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
                    'Successfully Fetched!!', data);

                res.json({'result': result})
            })
        });
    },
    getAllRolesWithCountFromUserRolesRelation: function (req, res) {

        var limit = req.query.limit ? req.query.limit : CONSTANTS.API.DEFAULT_LIMIT;
        var offset = req.query.offset ? req.query.offset : CONSTANTS.API.DEFAULT_OFFSET;
        var data, result = {};

        result = ResultResponses.failed(CONSTANTS.HTTP_CODES.SERVER_ERROR.INTERNAL_SERVER_ERROR,
            'Some Error Occurred.');

        Roles.find({}, {__v: 0}, {
            skip: parseInt(offset),
            limit: parseInt(limit)
        }, function (err, user_roles) {

            if (err) {
                console.log('Error While fetching Roles: RolesController->getAllRoles');
                throw err;
            }

            data = {
                limit: parseInt(limit),
                offset: parseInt(offset)
            }

            Roles.count({}, function (err, TotalCount) {

                data.count = TotalCount;
                data.Roles = user_roles;

                result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
                    'Successfully Fetched!!', data);

                res.json({'result': result})
            })
        });
    },
    deleteRoles: function (req, res) {

        var result = {};

        var role_id = req.params.role_id;

        Roles.findOne({_id: role_id}, function (err, role) {

            if (err) {
                console.log('Error: RolesController->deleteRoles');
                throw err;
            }

            if (!role) {

                result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
                    'role does not exists!!', null);
            } else {

                role.remove(function (err) {
                    if (err) {
                        console.log('OAuthClients Deletion Error: RolesController->deleteRoles');
                        throw err;
                    }
                });

                result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
                    'Successfully Deleted!!', role);
            }
            res.json({'result': result})
        })
    },
    postAssignRoleToUser: function (req, res) {

        var user_id = req.body._user_access_id;
        var role_id = req.body.role_id;

        UserAccess.findOne({_id: user_id}, function (err, user) {

            if (err) {
                console.log('Error While fetching UserAccess: UserRolesController->postAssignRoleToUser');
                throw err;
            }

            if (user) {


                Roles.findOne({_id: role_id}, function (err, role) {

                    if (err) {
                        console.log('Error While fetching Roles: UserRolesController->postAssignRoleToUser');
                        throw err;
                    }

                    if (role) {

                        User_To_Roles.findOne({_user_access_id: user_id}, function (err, user_role) {

                            if (err) {
                                console.log('Error While fetching Roles: UserRolesController->postAssignRoleToUser');
                                throw err;
                            }

                            if (!user_role) {
                                user_role = new User_To_Roles();
                                user_role._user_access_id = user_id;
                            }

                            user_role.role_id = role_id;
                            user_role.save(function (err) {
                                if (err) {
                                    console.log('Roles Save Error: UserRolesController->postAssignRoleToUser');
                                    throw err;
                                }

                                result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
                                    'Successfully Assigned!!', user_role);

                                res.json({'result': result})
                            });
                        })

                    } else {
                        result = ResultResponses.success(CONSTANTS.HTTP_CODES.CLIENT_ERROR.BAD_REQUEST,
                            'Role Not Found!!', role);

                        res.json({'result': result})
                    }
                })

            } else {
                result = ResultResponses.success(CONSTANTS.HTTP_CODES.CLIENT_ERROR.BAD_REQUEST,
                    'UserNot Found!!', user);

                res.json({'result': result})
            }
        })
    }
}