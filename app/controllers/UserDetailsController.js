var UserDetails = require('./../models').user_details,
    UserAccessDetails = require('./../models').user_access_details,
    UserAccess = require('./../models').user_access;
// loading user constants
var CONSTANTS = require('./../helpers/constants');
var ResultResponses = require('./../helpers/resultResponses');
var CheckUserType = require('./../helpers/checkUserType');
var AfterLogin = require('./../middlewares/after_login');
var OAuthAccessTokenController = require('./../controllers').OAuthAccessTokenController;
var request = require('request');
var ENV_File = require("./../../ENV.json");

module.exports = {

    postMember: function (req, res, next) {

        var email = req.body.email;
        var password = req.body.password;

        if (email)
            email = email.toLowerCase(); // Use lower-case e-mails to avoid case-sensitive e-mail matching

        UserAccess.findOne({
                $or: [{
                    'email': email
                }, {
                    'contact_number': req.body.contact_number
                }]
            },
            function (err, user) {

                var result;

                result = ResultResponses.failed(CONSTANTS.HTTP_CODES.SERVER_ERROR.INTERNAL_SERVER_ERROR,
                    'Some Error Occurred.');

                // if there are any errors, return the error
                if (err) {
                    console.log('Error in fetching User: passport->local-signup');
                    throw err;
                }

                // check to see if theres already a user with that email
                if (user) {

                    result = ResultResponses.failed(CONSTANTS.HTTP_CODES.CLIENT_ERROR.CONFLICT,
                        'Email Or Contact Number is already Registered.');

                    res.json({
                        'result': result
                    });
                }
                else {

                    // create the user
                    var newUser = new UserAccess();
                    newUser.email = email;
                    newUser.contact_number = req.body.contact_number;
                    newUser.country_code = req.body.country_code;
                    newUser.password = newUser.generateHash(password);

                    if (CheckUserType.checkIfTestEmail(email)) {

                        newUser.activated = CONSTANTS.USER_ACTIVATION.ACTIVATED;
                        newUser.activated_at = new Date();
                        newUser.activation_code = null;
                    }
                    else {

                        newUser.activated = CONSTANTS.USER_ACTIVATION.NOT_ACTIVATED;
                        newUser.activation_code = newUser.generateActivationCode(new Date());
                    }

                    newUser.save(function (err) {
                        if (err) {
                            console.log('Error in saving new User');
                            throw err;
                        }
                    });

                    var userDetails = new UserDetails();
                    userDetails.first_name = req.body.first_name;
                    userDetails.last_name = req.body.last_name;
                    userDetails._user_access_id = newUser._id;

                    userDetails.save(function (err) {
                        if (err) {
                            throw err;
                        }
                    });

                    var data = {
                        user: newUser,
                    }
                    // TODO - send email to the registered user for activation
                    result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
                        'Your Registration is successful.', data);
                    res.json({'result': result});
                }
            });
    },
    postLogin: function (req, res, next) {

        var email = req.body.username,
            password = req.body.password,
            client_id = req.body.client_id,
            client_secret = req.body.client_secret;

        var options = {
            url: ENV_File.ENVIRONMENT.APP_.URL + '/oauth/token',
            contentType: 'application/json',
            form: {
                grant_type: "password",
                username: email,
                password: password,
                client_id: client_id,
                client_secret: client_secret,
            }
        };

        request.post(options, function (err, response, body) {

            if (err) {
                console.log("Error in Fetching Access Token -> userDetailsController : postLogin")
                throw err;
            }
            res.json(body)
        })
    },
    getMemberInfo: function (req, res, next) {

        var result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
            'Successfully Fetched User!!', req.member);
        res.json({
            'result': result
        })
    },
    deleteMemberInfo: function (req, res) {

        var result = {};

        result = ResultResponses.failed(CONSTANTS.HTTP_CODES.SERVER_ERROR.INTERNAL_SERVER_ERROR,
            'Some Error Occurred.');

        UserAccess.findOne({
            _id: req.member._id
        }, function (err, user) {

            if (err) {
                console.log('User Delete Error: userDetailsController->deleteMemberInfo');
                throw err;
            }

            if (user) {

                user.deleted_at = new Date();
                user.save(function (err) {

                    if (err) {
                        console.log('User Delete update Error: userDetailsController->deleteMemberInfo');
                        throw err;
                    }
                });

                result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
                    'Successfully Deleted!!', user);
            }
            else {

                result = ResultResponses.invalid(CONSTANTS.HTTP_CODES.CLIENT_ERROR.UNAUTHORISED,
                    'User not found.!!');
            }

            res.json({
                'result': result
            })
        });
    },
    putMemberInfo: function (req, res) {

        var result = {};

        result = ResultResponses.failed(CONSTANTS.HTTP_CODES.SERVER_ERROR.INTERNAL_SERVER_ERROR,
            'Some Error Occurred.');

        UserDetails.findOne({
            _id: req.member.member_details_id
        }, function (err, user) {

            if (err) {
                console.log('User Update Error: userDetailsController');
                throw err;
            }

            if (user) {
                user.first_name = req.body.first_name;
                user.last_name = req.body.last_name;
                user.nick_name = req.body.nick_name;
                user.sex = req.body.sex;
                user.show_dob = req.body.show_dob;
                user.dob = new Date(req.body.dob);
                user.save(function (err) {
                    if (err) {
                        console.log('User Save Error: userDetailsController');
                        throw err;
                    }
                });
                result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
                    'Details Successfully Updated!!', user);
            }
            else {
                result = ResultResponses.invalid(CONSTANTS.HTTP_CODES.CLIENT_ERROR.UNAUTHORISED,
                    'User not found.!!');
            }
            res.json({'result': result})
        });
    },
    postProfilePic: function (req, res, next) {
        // Everything went fine
        result = ResultResponses.failed(CONSTANTS.HTTP_CODES.SERVER_ERROR.INTERNAL_SERVER_ERROR,
            'Some Error Occurred.');

        userDetails.findOne({
            _id: req.member.member_details_id
        }, function (err, user) {

            if (err) {
                console.log('ProfilePIC User Cannot Find Error: userDetailsController');
                throw err;
            }

            if (user) {

                console.log(req.body);
                console.log(req.params);

                user.profile_pic = req.file.filename;
                user.save(function (err) {

                    if (err) {
                        console.log('ProfilePIC Save Error: userDetailsController');
                        throw err;
                    }
                });

                result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
                    'Profile Pic Successfully Updated!!', user);
            }
            else {

                result = ResultResponses.invalid(CONSTANTS.HTTP_CODES.CLIENT_ERROR.UNAUTHORISED,
                    'User not found.!!');
            }

            res.json({
                'result': result
            })
        });
    },
    postAuthenticatePassword: function (req, res, next) {

        var password = req.body.password,
            email = req.body.email,
            result = null;

            result = ResultResponses.failed(CONSTANTS.HTTP_CODES.SERVER_ERROR.INTERNAL_SERVER_ERROR,
                'Some Error Occurred.');

        UserAccess.findOne({email: email}, function (err, userAccess) {

            if (err) {
                console.log('UserAccess Error: UserAccessController->postChangePassword');
                throw err;
            }

            if (userAccess) {

                if (userAccess.validPassword(password)) {
                        result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK, 'Valid Password!!');
                } else {
                    result = ResultResponses.invalid(CONSTANTS.HTTP_CODES.CLIENT_ERROR.BAD_REQUEST,
                        'Invalid Password');
                }
            } else {

                result = ResultResponses.invalid(CONSTANTS.HTTP_CODES.CLIENT_ERROR.BAD_REQUEST,
                    'User Not Found');
            }
            res.json({'result': result});
        })
    }
}
