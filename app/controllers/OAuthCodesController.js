var userDetails = require('./../models/user_details');
var Notifications = require('./../models/notifications');
// loading user constants
var CONSTANTS = require('./../helpers/constants');
var ResultResponses = require('./../helpers/resultResponses');
var OAuthClients = require('./../models/oauth_clients');
var OAuthCodes = require('./../models/oauth_codes');
var bcrypt = require('bcrypt-nodejs');
var randomstring = require("randomstring");


module.exports = {

    getOAuthCodes: function (req, res) {

        var limit = req.query.limit ? req.query.limit : CONSTANTS.API.DEFAULT_LIMIT;
        var offset = req.query.offset ? req.query.offset : CONSTANTS.API.DEFAULT_OFFSET;
        var data, result = {};

        result = ResultResponses.failed(CONSTANTS.HTTP_CODES.SERVER_ERROR.INTERNAL_SERVER_ERROR,
            'Some Error Occurred.');

        OAuthCodes.find({}, {__v: 0}, {
            skip: parseInt(offset),
            limit: parseInt(limit),
            sort: {
                activity_time: 1 //Sort by activity_time Added ASC
            }
        }, function (err, oauthCodes) {

            if (err) {
                console.log('Error While fetching OAuthCodes: OAuthCodesController->getOAuthCodes');
                throw err;
            }

            data = {
                limit: parseInt(limit),
                offset: parseInt(offset)
            }

            OAuthCodes.count({}, function (err, TotalCount) {

                data.count = TotalCount;
                data.oauthCodes = oauthCodes;

                result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
                    'Successfully Fetched!!', data);

                res.json({'result': result})
            })
        });
    },

    postOAuthCodes: function (req, res) {

        var result = {};

        var client_id = req.body.client_id;
        var user_id = req.body.user_id;
        var redirect = req.body.redirect_url;
        var scopes = req.body.scopes;
        var is_revoked = req.body.is_revoked;

        var oauthCodes = new OAuthCodes();
        oauthCodes.client_id = client_id;
        oauthCodes._user_access_id = user_id;
        oauthCodes.redirect_url = redirect;
        oauthCodes.is_revoked = is_revoked;
        oauthCodes.scopes = scopes;
        oauthCodes.code = oauthCodes.generateCode();

        oauthCodes.save(function (err) {
            if (err) {
                console.log('OAuthCodes Save Error: OAuthCodesController->postOAuthCodes');
                throw err;
            }
        });

        result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
            'Successfully Created!!', oauthCodes);

        res.json({'result': result})
    },

    deleteOAuthCodes: function (req, res) {

        var result = {};

        var code_id = req.body.code_id;

        OAuthCodes.findOne({_id: code_id}, function (err, oauthCode) {

            if (err) {
                console.log('Error: OAuthCodeController->deleteOAuthCodes');
                throw err;
            }

            if (!oauthCode) {

                result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
                    'Client does not exists!!', null);
            } else {

                oauthCode.remove(function (err) {
                    if (err) {
                        console.log('OAuthClients Deletion Error: OAuthCodeController->deleteOAuthCodes');
                        throw err;
                    }
                });

                result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
                    'Successfully Deleted!!', oauthCode);
            }
            res.json({'result': result})
        })
    }
}