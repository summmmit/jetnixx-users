var util = require('util');
var ResultResponses = require('./../../helpers/resultResponses');
// loading user constants
var CONSTANTS = require('./../../helpers/constants');
module.exports = {
    checkPostOAuthClient: function (req, res, next) {
        var result = {};

        var user_id = req.body.user_id;
        var redirect_url = req.body.redirect_url;
        var client_name = req.body.client_name;
        var client_type = req.body.client_type;

        req.checkBody('client_type', 'client_type is Required').notEmpty();
        req.checkBody('user_id', 'user_id is Required').notEmpty();
        req.checkBody('client_name', 'client_name is Required').notEmpty();
        req.checkBody('redirect_url', 'redirect_url is Required').notEmpty();

        var errors = req.validationErrors();
        if (errors) {
            result = ResultResponses.validationError(CONSTANTS.HTTP_CODES.CLIENT_ERROR.NOT_ACCEPTABLE,
                'Invalid Request Parameters', errors);
            return res.json({
                'result': result
            });
        }
        next();
    }
}
