module.exports = {
    OAuthValidator: require('./OAuthValidator'),
    ContactNumbersSettingsValidator: require('./ContactNumbersSettingsValidator'),
    LabelSettingsValidator: require('./LabelsSettingsValidator'),
    userAccessValidator: require('./userAccessValidator'),
}
