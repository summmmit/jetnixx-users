const util = require('util');
var environment = require('./env').environment;
var database = function (ENV) {
    console.log("--------------------Application Information----------------------------------");
    console.log('Database HOST: %s', ENV.MONGO_.DB_HOST);
    console.log('Database PORT: %s', ENV.MONGO_.DB_PORT);
    console.log('Database USERNAME: %s', ENV.MONGO_.DB_USERNAME);
    console.log('Database PASSWORD: %s', ENV.MONGO_.DB_PASSWORD);
    console.log('Database NAME: %s', ENV.MONGO_.DB_DATABASE_NAME);
    return util.format('mongodb://%s:%s@%s:%s/%s', ENV.MONGO_.DB_USERNAME, ENV.MONGO_.DB_PASSWORD, ENV.MONGO_.DB_HOST, ENV.MONGO_.DB_PORT, ENV.MONGO_.DB_DATABASE_NAME);
    //return util.format('mongodb://s:%s/%s', ENV.MONGO_.DB_HOST, ENV.MONGO_.DB_PORT, ENV.MONGO_.DB_DATABASE_NAME);
    //return util.format('mongodb://%s:%s/%s', ENV.MONGO_.DB_HOST, 27017, ENV.MONGO_.DB_DATABASE_NAME);
}

module.exports = {
    ENV: environment,
    DATABASE: database(environment)
}
